# Use the official Rust image for the building stage.
FROM rust:latest as builder

# Create a new empty shell project
RUN USER=root cargo new --bin p10
WORKDIR /p10

# Copy our manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# This build step will cache your dependencies
RUN cargo build --release
RUN rm src/*.rs

# Now that the dependencies are built, copy your source code
ADD . /p10

# Build for release.
RUN rm ./target/release/deps/p10*
RUN cargo build --release

# Final base
FROM debian:buster-slim

ARG APP=/usr/src/app

EXPOSE 8080

# Install necessary packages
RUN apt-get update \
    && apt-get install -y ca-certificates tzdata openssl libssl-dev \
    && rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt install -y openssl
ENV TZ=Etc/UTC \
    APP_USER=appuser

# Create appuser
RUN groupadd $APP_USER \
    && useradd -g $APP_USER $APP_USER \
    && mkdir -p ${APP}

WORKDIR ${APP}

# Copy our build
COPY --from=builder /p10/target/release/p10 ${APP}/p10

RUN chown -R $APP_USER:$APP_USER ${APP}

USER $APP_USER
CMD [ "p10" ]
