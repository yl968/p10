# Rust AWS Lambda Function Deployment Using Docker

This README provides a step-by-step guide on how to deploy a Rust-based AWS Lambda function that utilizes Hugging Face models for NLP tasks, dockerize the Lambda project, deploy it using ECR, and expose the function via a Function URL or API Gateway.



## Step 1: Create the Rust Project

Initialize a new Cargo project that depends on `rust-bert` or `rustformers/llm`.
Update the Cargo.toml and main.rs file.

## Step 2: Dockerize the Lambda Project
Create a Dockerfile in the root of your project
![image](702b70e9fc94110cf9a8699563855db.png)

## Step 3: Build and Push the Docker Image to Amazon ECR
```bash
aws ecr create-repository --repository-name p10-repo --region us-east-1
```
Result in the repository:
```bash
{
    "repository": {
        "repositoryArn": "arn:aws:ecr:us-east-1:339712872070:repository/p10-repo",
        "registryId": "339712872070",
        "repositoryName": "p10-repo",
        "repositoryUri": "339712872070.dkr.ecr.us-east-1.amazonaws.com/p10-repo",
        "createdAt": "2024-04-11T01:40:59.653000-04:00",
        "imageTagMutability": "MUTABLE",
        "imageScanningConfiguration": {
            "scanOnPush": false
        },
        "encryptionConfiguration": {
            "encryptionType": "AES256"
        }
    }
}
```
Tag and Push to ECR
```bash
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 339712872070.dkr.ecr.us-east-1.amazonaws.com
```
![image](10a2e829434cdb40ed8ceb8f228f761.png)


## Step 4: Deploy the Docker Image as a Lambda Function
Open the AWS Lambda Console:
Navigate to AWS Lambda in your AWS Management Console.

Create Function:
Click on “Create function”.
Select “Container image”.
Enter a function name (e.g., p10-function).
Click on “Browse images” to select your container image from ECR. You should find p10-repo and the latest tag. Select this image.
Click on “Save”.

Deploy the Function:
After configuring the settings, click on “Create function”.
![image](ab6914148e1e360757d685b8588cf42.png)

## Explanation
At the docker step, I meet this problem: 2024-04-11 01:35:31 ./p10: error while loading shared libraries: libssl.so.3: cannot open shared object file: No such file or directory
![image](e59f79f28a92b2b2cd782ff56d1ab7e.png)

I tried to add the installation line:
```bash
RUN apt-get update && apt install -y openssl
```

But this problem still exists, and this leads to the failure of testing. However, I think I did the rest of process correctly. The final url: https://4bwvlvpdbacmpgbcxfhd54pf4e0dswgr.lambda-url.us-east-1.on.aws/
