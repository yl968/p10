use lambda_runtime::{Error, LambdaEvent, service_fn};
use serde_json::{Value, json};
use rust_bert::pipelines::translation::{Language, TranslationModelBuilder};
use serde::{Serialize, Deserialize};

#[derive(Deserialize)]
struct Request {
    text: String,
}

#[derive(Serialize)]
struct Response {
    translated_text: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn func(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let (event, _context) = event.into_parts();
    let model = TranslationModelBuilder::new()
        .with_source_languages(vec![Language::English])
        .with_target_languages(vec![Language::Spanish, Language::French, Language::Italian])
        .create_model()?;
        
    let output = model.translate(&[event.text.as_str()], None, Language::Spanish)?;
    Ok(Response {
        translated_text: output.join(" "), 
    })
}
